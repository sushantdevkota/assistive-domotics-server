require 'test_helper'

class SugarLevelsControllerTest < ActionController::TestCase
  setup do
    @sugar_level = sugar_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sugar_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sugar_level" do
    assert_difference('SugarLevel.count') do
      post :create, sugar_level: { checkup_time: @sugar_level.checkup_time, mg_per_dl: @sugar_level.mg_per_dl, user_id: @sugar_level.user_id }
    end

    assert_redirected_to sugar_level_path(assigns(:sugar_level))
  end

  test "should show sugar_level" do
    get :show, id: @sugar_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sugar_level
    assert_response :success
  end

  test "should update sugar_level" do
    patch :update, id: @sugar_level, sugar_level: { checkup_time: @sugar_level.checkup_time, mg_per_dl: @sugar_level.mg_per_dl, user_id: @sugar_level.user_id }
    assert_redirected_to sugar_level_path(assigns(:sugar_level))
  end

  test "should destroy sugar_level" do
    assert_difference('SugarLevel.count', -1) do
      delete :destroy, id: @sugar_level
    end

    assert_redirected_to sugar_levels_path
  end
end
