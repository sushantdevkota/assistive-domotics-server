json.array!(@sugar_levels) do |sugar_level|
  json.extract! sugar_level, :id, :user_id, :mg_per_dl, :checkup_time
  json.url sugar_level_url(sugar_level, format: :json)
end
