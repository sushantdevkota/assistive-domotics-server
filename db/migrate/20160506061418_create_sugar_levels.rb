class CreateSugarLevels < ActiveRecord::Migration
  def change
    create_table :sugar_levels do |t|
      t.integer :user_id
      t.integer :mg_per_dl
      t.datetime :checkup_time

      t.timestamps null: false
    end
  end
end
