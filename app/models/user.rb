class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :uuid, uniqueness: true

  has_many :images
  has_many :blood_pressures
  has_many :sugar_levels
  has_many :contacts
  has_many :fall_detections
  accepts_nested_attributes_for :images
end
