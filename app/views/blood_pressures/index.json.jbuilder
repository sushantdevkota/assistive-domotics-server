json.array!(@blood_pressures) do |blood_pressure|
  json.extract! blood_pressure, :id, :user_id, :systolic, :diastolic, :checkup_time
  json.url blood_pressure_url(blood_pressure, format: :json)
end
