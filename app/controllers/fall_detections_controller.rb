class FallDetectionsController < ApplicationController
  before_action :set_fall_detection, only: [:show, :edit, :update, :destroy]

  # GET /fall_detections
  # GET /fall_detections.json
  def index
    @fall_detections = FallDetection.where(user_id: current_user.id)
  end

  # GET /fall_detections/1
  # GET /fall_detections/1.json
  def show
  end

  # GET /fall_detections/new
  def new
    @fall_detection = FallDetection.new
  end

  # GET /fall_detections/1/edit
  def edit
  end

  # POST /fall_detections
  # POST /fall_detections.json
  def create
    @fall_detection = FallDetection.new(fall_detection_params)
    @fall_detection.user_id = current_user.id
    respond_to do |format|
      if @fall_detection.save
        format.html { redirect_to @fall_detection, notice: 'Fall detection was successfully created.' }
        format.json { render :show, status: :created, location: @fall_detection }
      else
        format.html { render :new }
        format.json { render json: @fall_detection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fall_detections/1
  # PATCH/PUT /fall_detections/1.json
  def update
    respond_to do |format|
      if @fall_detection.update(fall_detection_params)
        format.html { redirect_to @fall_detection, notice: 'Fall detection was successfully updated.' }
        format.json { render :show, status: :ok, location: @fall_detection }
      else
        format.html { render :edit }
        format.json { render json: @fall_detection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fall_detections/1
  # DELETE /fall_detections/1.json
  def destroy
    @fall_detection.destroy
    respond_to do |format|
      format.html { redirect_to fall_detections_url, notice: 'Fall detection was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_by_uuid
    @fall_detection = FallDetection.new(fall_detection_params)
    uuid = params[:uuid]
    user = User.find_by_uuid(uuid)
    @fall_detection.user_id=user.id
    respond_to do |format|
      if @fall_detection.save
        format.html { redirect_to @fall_detection, notice: 'Fall detection was successfully created.' }
        format.json { render :show, status: :created, location: @fall_detection }
      else
        format.html { render :new }
        format.json { render json: @fall_detection.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fall_detection
      @fall_detection = FallDetection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fall_detection_params
      params.require(:fall_detection).permit(:user_id, :detected_time)
    end
end
