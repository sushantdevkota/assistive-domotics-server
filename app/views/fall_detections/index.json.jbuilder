json.array!(@fall_detections) do |fall_detection|
  json.extract! fall_detection, :id, :user_id, :detected_time
  json.url fall_detection_url(fall_detection, format: :json)
end
