class CreateBloodPressures < ActiveRecord::Migration
  def change
    create_table :blood_pressures do |t|
      t.integer :user_id
      t.float :systolic
      t.float :diastolic
      t.datetime :checkup_time

      t.timestamps null: false
    end
  end
end
