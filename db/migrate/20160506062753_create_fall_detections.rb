class CreateFallDetections < ActiveRecord::Migration
  def change
    create_table :fall_detections do |t|
      t.integer :user_id
      t.datetime :detected_time

      t.timestamps null: false
    end
  end
end
