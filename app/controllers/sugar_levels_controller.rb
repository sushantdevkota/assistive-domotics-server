class SugarLevelsController < ApplicationController
  before_action :set_sugar_level, only: [:show, :edit, :update, :destroy]

  # GET /sugar_levels
  # GET /sugar_levels.json
  def index
    @sugar_levels = SugarLevel.where(user_id: current_user.id)
  end

  # GET /sugar_levels/1
  # GET /sugar_levels/1.json
  def show
  end

  # GET /sugar_levels/new
  def new
    @sugar_level = SugarLevel.new
  end

  # GET /sugar_levels/1/edit
  def edit
  end

  # POST /sugar_levels
  # POST /sugar_levels.json
  def create
    @sugar_level = SugarLevel.new(sugar_level_params)
    @sugar_level.user_id = current_user.id
    respond_to do |format|
      if @sugar_level.save
        format.html { redirect_to @sugar_level, notice: 'Sugar level was successfully created.' }
        format.json { render :show, status: :created, location: @sugar_level }
      else
        format.html { render :new }
        format.json { render json: @sugar_level.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_by_uuid
    @sugar_level = SugarLevel.new(sugar_level_params)
    uuid = params[:uuid]
    user = User.find_by_uuid(uuid)
    @sugar_level.user_id=user.id
    respond_to do |format|
      if @sugar_level.save
        format.html { redirect_to @sugar_level, notice: 'Sugar level was successfully created.' }
        format.json { render :show, status: :created, location: @sugar_level }
      else
        format.html { render :new }
        format.json { render json: @sugar_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sugar_levels/1
  # PATCH/PUT /sugar_levels/1.json
  def update
    @sugar_level.user_id = current_user.id
    respond_to do |format|
      if @sugar_level.update(sugar_level_params)
        format.html { redirect_to @sugar_level, notice: 'Sugar level was successfully updated.' }
        format.json { render :show, status: :ok, location: @sugar_level }
      else
        format.html { render :edit }
        format.json { render json: @sugar_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sugar_levels/1
  # DELETE /sugar_levels/1.json
  def destroy
    @sugar_level.destroy
    respond_to do |format|
      format.html { redirect_to sugar_levels_url, notice: 'Sugar level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sugar_level
      @sugar_level = SugarLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sugar_level_params
      params.require(:sugar_level).permit(:user_id, :mg_per_dl, :checkup_time)
    end
end
