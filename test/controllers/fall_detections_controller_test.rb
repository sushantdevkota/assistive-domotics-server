require 'test_helper'

class FallDetectionsControllerTest < ActionController::TestCase
  setup do
    @fall_detection = fall_detections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fall_detections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fall_detection" do
    assert_difference('FallDetection.count') do
      post :create, fall_detection: { detected_time: @fall_detection.detected_time, user_id: @fall_detection.user_id }
    end

    assert_redirected_to fall_detection_path(assigns(:fall_detection))
  end

  test "should show fall_detection" do
    get :show, id: @fall_detection
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fall_detection
    assert_response :success
  end

  test "should update fall_detection" do
    patch :update, id: @fall_detection, fall_detection: { detected_time: @fall_detection.detected_time, user_id: @fall_detection.user_id }
    assert_redirected_to fall_detection_path(assigns(:fall_detection))
  end

  test "should destroy fall_detection" do
    assert_difference('FallDetection.count', -1) do
      delete :destroy, id: @fall_detection
    end

    assert_redirected_to fall_detections_path
  end
end
