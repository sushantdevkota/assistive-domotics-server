class GraphController < ApplicationController
  def index
  end

  def blood_pressure_data
    respond_to do |format|
      format.json {
        render :json => [1,2,3,4,5]
      }
    end
  end

  def sugar_level_data
      respond_to do |format|
        format.json {
          render :json => [1,2,3,4,5]
        }
      end
    end
end